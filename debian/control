Source: chafa
Section: graphics
Homepage: https://hpjansson.org/chafa/
Priority: optional
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/debian/chafa.git
Vcs-Browser: https://salsa.debian.org/debian/chafa
Maintainer: Mo Zhou <lumin@debian.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               docbook,
               gtk-doc-tools,
               libwebp-dev,
               libxml2-utils,
               libglib2.0-dev,
               libfreetype-dev,
               libavif-dev,
               libjpeg-dev,
               librsvg2-dev,
               libtiff-dev,
Rules-Requires-Root: no

Package: chafa
Architecture: any
Multi-Arch: foreign
Depends: libchafa0t64 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: Image-to-text converter supporting a wide range of symbols, etc.
 Chafa is a command-line utility that converts all kinds of images, including
 animated image formats like GIFs, into ANSI/Unicode character output that can
 be displayed in a terminal.
 .
 It is highly configurable, with support for alpha transparency and multiple
 color modes and color spaces, combining a range of Unicode characters for
 optimal output.
 .
 The core functionality is provided by a C library with a public,
 well-documented API.
 .
 This package ships the command line tool.

Package: libchafa0t64
Provides: ${t64:Provides}
Replaces: libchafa0
Breaks: libchafa0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library for image-to-text converter chafa
 Chafa is a command-line utility that converts all kinds of images, including
 animated image formats like GIFs, into ANSI/Unicode character output that can
 be displayed in a terminal.
 .
 It is highly configurable, with support for alpha transparency and multiple
 color modes and color spaces, combining a range of Unicode characters for
 optimal output.
 .
 The core functionality is provided by a C library with a public,
 well-documented API.
 .
 This package ships the shared object.

Package: libchafa-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libchafa0t64 (= ${binary:Version}), ${misc:Depends}
Description: development files for image-to-text converter chafa
 Chafa is a command-line utility that converts all kinds of images, including
 animated image formats like GIFs, into ANSI/Unicode character output that can
 be displayed in a terminal.
 .
 It is highly configurable, with support for alpha transparency and multiple
 color modes and color spaces, combining a range of Unicode characters for
 optimal output.
 .
 The core functionality is provided by a C library with a public,
 well-documented API.
 .
 This package ships the development files.
